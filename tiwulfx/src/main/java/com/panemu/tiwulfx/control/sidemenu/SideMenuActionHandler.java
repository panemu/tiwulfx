/*
 * License GNU LGPL
 * Copyright (C) 2013 Amrullah .
 */
package com.panemu.tiwulfx.control.sidemenu;

/**
 *
 * @author Amrullah 
 */
public interface SideMenuActionHandler {
	
	public void executeAction(String actionName);
    
}
